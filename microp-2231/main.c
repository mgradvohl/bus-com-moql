/* ---------------------------------------------------------------------------
 * Carte LAUCHPAD Rev.1.5 - MSP430G2553 - ESIGELEC
 * UltraSonic Sensor SRF05 (or aq.) + Carte d'extension
 * Vcc (3.3V)
 * ---------------------------------------------------------------------------
 * MSP430G2531
 * | 1 |Vcc Vss | 20 |
 * P1.0 -<LED| 2 | | 19 | P2.6-]
 * P1.1 | 3 | | 18 | P2.7]
 * P1.2-servomotor | 4 | | 17 | Tst
 * P1.3  | 5 | | 16 | !RST
 * P1.4 -<trigger | 6 | | 15 | P1.7<-miso
 * P1.5 | 7 | | 14 | P1.6-mosi
 * P2.0 | 8 | | 13 | P2.5-<LED]
 * [Echo>-P2.1 | 9 | | 12 | P2.4-<LED]
 * P2.2 | 10 | | 11 | P2.3
 *
 * ---------------------------------------------------------------------------
 */
 #include <msp430.h>
#include <intrinsics.h>

volatile unsigned char RXDta;

 #define MEASUREMENT_PERIOD 3500
 #define TRIGGER_PULSE 150
 #define MOTORS_FREQUENCY 33333 // motors period T=30 ms (0.0333 s) soit 30 Hz

 unsigned int t1 = 0,
 t2 = 0,
 distance = 0;
 int i;

 unsigned long delay_measurement = 0;

 /* ----------------------------------------------------------------------------
 * Fonction d'initialisation de la carte TI LauchPAD
 * Entrees: -
 * Sorties: -
 */
 void init_BOARD( void )
 {
     // Stop watchdog timer to prevent time out reset
     WDTCTL = WDTPW | WDTHOLD;

     if( (CALBC1_1MHZ == 0xFF) || (CALDCO_1MHZ == 0xFF) )
     {
         __bis_SR_register(LPM4_bits); // #trap
     }
     else
     {
        // Factory Set.
         BCSCTL1 = CALBC1_1MHZ;
         DCOCTL = CALDCO_1MHZ;
     }


     //--------------- Secure mode
     P1SEL = 0x00; // GPIO
     P2SEL = 0x00; // GPIO
     P1DIR = 0x00; // IN
     P2DIR = 0x00; // IN

//     // partie spi
//     // USI Config. for SPI 3 wires Slave Op.
//      // P1SEL Ref. p41,42 SLAS694J used by USIPEx
//      USICTL0 |= USISWRST;
//      USICTL1 = 0;



//      // 3 wire, mode Clk&Ph / 14.2.3 p400
//      // SDI-SDO-SCLK - LSB First - Output Enable - Transp. Latch
//      USICTL0 |= (USIPE7 | USIPE6 | USIPE5 | USILSB | USIOE | USIGE );
//      // Slave Mode SLAU144J 14.2.3.2 p400
//      USICTL0 &= ~(USIMST);
//      USICTL1 |= USIIE;
//      USICTL1 &= ~(USICKPH | USII2C);
//
//      USICKCTL = 0;           // No Clk Src in slave mode
//      USICKCTL &= ~(USICKPL | USISWCLK);  // Polarity - Input ClkLow
//
//      USICNT = 0;
//      USICNT &= ~(USI16B | USIIFGCC ); // Only lower 8 bits used 14.2.3.3 p 401 slau144j
//      USISRL = 0x23;  // hash, just mean ready; USISRL Vs USIR by ~USI16B set to 0
//      USICNT = 0x08;

     // inititialisation des led warning
     P1DIR |= (BIT0);
     P1OUT &= ~(BIT0);

     // GPIO initialisation
     P1SEL &= ~(BIT4); // US-SRF Trigger Line out
     P1DIR |= (BIT4);
     P1OUT &= ~BIT4;

     P1SEL &= ~(BIT3); // LaunchPad Button
     P1DIR &= ~(BIT3);

     //servomotor
     P1DIR |= BIT2;
     P1SEL |= BIT2;

     P2SEL |= BIT1; // US-SRF Echo line input capture
     P2DIR &= ~BIT1;
     P1DIR |= BIT7;
 }

 /* ----------------------------------------------------------------------------
 * Fonction d'initialisation du TIMER
 * Entree : -
 * Sorties: -
 */
 void init_Timer( void )
 {
     TACTL &= ~MC_0; // arret du timer
     TACCR0 = MOTORS_FREQUENCY; // periode du signal PWM 2KHz
     TACTL = (TASSEL_2 | MC_1 | ID_0 | TACLR); // select TimerA source SMCLK, set mode to up-counting
     TACCTL1 = OUTMOD_7; // select timer compare mode
 }

 /* ----------------------------------------------------------------------------
 * Fonction RADAR en utilisant les 4 LED de la carte d'extension
 * Entree : -
 * Sorties: -
 */
 void radar( void )
 {
     if (distance <= 30)
     {
         P1OUT |= (BIT0);
     }
     else if (distance <= 50)
     {
         P1OUT |= (BIT0);
     }
     else if (distance <= 70)
     {
         P1OUT &= ~(BIT0);
     }
     else if (distance <= 90)
     {
         P1OUT |= (BIT0);
     }
     else
     {
         P1OUT &= ~(BIT0);
     }

     //Aff_valeur(convert_Hex_Dec(distance));
 }

/*
* main.c
 *
 */
 void main(void)
 {
     init_BOARD();
     __no_operation();
     init_Timer();
     __enable_interrupt();


     while(1)
     {

         radar();
         TACCR0 = 20000;  //PWM period
         for (i=350; i<=2350; i++) {
             TACCR1 = i;
             TACCTL1 = OUTMOD_7;  //CCR1 selection reset-set
             TACTL = TASSEL_2|MC_1;   //SMCLK submain clock,upmode
         }
     }
 }

 // --------------------------- R O U T I N E S D ' I N T E R R U P T I O N S
  /* ************************************************************************* */
  /* VECTEUR INTERRUPTION TIMER0 */
  /* ************************************************************************* */
  #pragma vector = TIMER0_A0_VECTOR // Timer A Interrupt Service Routine
  __interrupt void timer0_A0_isr(void)
  {
      ( delay_measurement < MEASUREMENT_PERIOD ) ? ( delay_measurement++ ) : (delay_measurement = 0 );

      if( !delay_measurement )
      {
          P1OUT |= BIT4; // Trigger ON
          P1OUT &= ~(BIT2);
      }
      else
      {
          P1OUT &= ~BIT4; // trigger OFF
      }
      TACTL &= ~TAIFG;
      TACCTL0 &= ~CCIFG; // unnecessary CCR0 has only one src

      switch(_even_in_range(TAIV,TAIV_TAIFG))
      {
         case TAIV_NONE:
             break; // no more interrupts pending, exit ISR here.
         case TAIV_TACCR1: { // CCR1 interrupt - 0x0002
             if ( P2IN & BIT1 )
             { // detect rising edge
                 t1 = TACCR1;
             }
             else { // and falling edge
                 t2 = TACCR1;
                 if (t2 > t1)
                 {
                     distance = (t2-t1)/59;
                 }
                 else
                 {
                     distance = 0;
                 }
             }
             break;
        }
       //case TAIV_TACCR2:
       //break; // CCR2 interrupt - 0x0004
       //case TAIV_6: // reserved
       //break;
       //case TAIV_8: // reserved
      // break;
       //case TAIV_TAIFG: // 0x000A
       //break; // timer overflow
       //default:
       //break;
     }

     TACTL &= ~TAIV_TAIFG;
     TACCTL1 &= ~CCIFG;
  }

  // --------------------------- R O U T I N E S   D ' I N T E R R U P T I O N S

  /* ************************************************************************* */
  /* VECTEUR INTERRUPTION USI                                                  */
  /* ************************************************************************* */
  #pragma vector=USI_VECTOR
  __interrupt void universal_serial_interface(void)
  {
      while( !(USICTL1 & USIIFG) );   // waiting char by USI counter flag
      RXDta = USISRL;

      if (RXDta == 0x31) //if the input buffer is 0x31 (mainly to read the buffer)
      {
          P1OUT |= BIT0; //turn on LED
      }
      else if (RXDta == 0x30)
      {
          P1OUT &= ~BIT0; //turn off LED
      }
      USISRL = RXDta;
      USICNT &= ~USI16B;  // re-load counter & ignore USISRH
      USICNT = 0x08;      // 8 bits count, that re-enable USI for next transfert
  }
  //------------------------------------------------------------------ End ISR
