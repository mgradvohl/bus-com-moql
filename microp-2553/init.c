/*
 * init.c
 */

#include "init.h"

/* ----------------------------------------------------------------------------
 * Fonction d'initialisation de la carte TI LauchPAD
 * Entrees: -
 * Sorties:  -
 */
void init_BOARD( void )
{

    WDTCTL = WDTPW | WDTHOLD;
    BCSCTL1= CALBC1_1MHZ;       // frequence d�horloge 1MHz
    DCOCTL= CALDCO_1MHZ;        // frequence d�horloge 1MHz

        //--------------- Secure mode
    P1SEL  = 0x00;        // GPIO
    P1SEL2 = 0x00;        // GPIO
    P2SEL  = 0x00;        // GPIO
    P2SEL2 = 0x00;        // GPIO
    P1DIR  = 0x00;        // IN
    P2DIR  = 0x00;        // IN

    P1SEL  &= ~LED_R;
    P1SEL2 &= ~LED_R;
    P1DIR  |=  LED_R;  // LED: output
    P1OUT  &= ~LED_R;

    // moteur A
    P2DIR |= BIT2;              // P2.2 en sortie
    P2SEL |= BIT2;              // selection fonction TA1.1
    P2SEL2 &= ~BIT2;            // selection fonction TA1.1

    // moteur B
    P2DIR |= BIT4;              // P2.4 en sortie
    P2SEL |= BIT4;              // selection fonction TA1.2
    P2SEL2 &= ~BIT4;            // selection fonction TA1.2

    // parametres generales PWM
    P2DIR |= BIT5;              // P2.5 en sortie
    P2DIR |= BIT1;              // P2.1 en sortie
    TA1CTL = TASSEL_2 | MC_1;   // source SMCLK pour TimerA , mode comptage Up
    TA1CCTL1 |= OUTMOD_7;       // activation mode de sortie n�7 sur TA1.1
    TA1CCTL2 |= OUTMOD_7;       // activation mode de sortie n�7 sur TA1.2
    TA1CCR0 = 300;              // determine la periode du signal
    TA1CCR1 = 0;               // determine le rapport cyclique du signal TA1.1 // moteur gauche
    TA1CCR2 = 0;               // determine le rapport cyclique du signal TA1.2 // moteur droite

}

/* ----------------------------------------------------------------------------
 * Fonction d'initialisation de l'UART
 * Entree : -
 * Sorties: -
 */
void init_UART( void )
{
    P1SEL  |= (BIT1 | BIT2);                    // P1.1 = RXD, P1.2=TXD
    P1SEL2 |= (BIT1 | BIT2);                    // P1.1 = RXD, P1.2=TXD
    UCA0CTL1 |= UCSWRST;                        // SOFTWARE RESET
    UCA0CTL1 |= UCSSEL_2;                       // SMCLK (2 - 3)
    UCA0BR0 = 104;                             // 104 1MHz, OSC16, 9600 (8Mhz : 52) : 8 115k - 226/12Mhz
    UCA0BR1 = 0;                                // 1MHz, OSC16, 9600 - 4/12Mhz
    UCA0MCTL = 10;
    UCA0CTL0 &= ~(UCPEN  | UCMSB | UCDORM);
    UCA0CTL0 &= ~(UC7BIT | UCSPB  | UCMODE_3 | UCSYNC); // dta:8 stop:1 usci_mode3uartmode
    UCA0CTL1 &= ~UCSWRST;                       // **Initialize USCI state machine**
    /* Enable USCI_A0 RX interrupt */
    IE2 |= UCA0RXIE;
}

/* ----------------------------------------------------------------------------
 * Fonction d'initialisation de l'USCI POUR SPI SUR UCB0
 * Entree : -
 * Sorties: -
 */
void init_USCI( void )
{
    // Waste Time, waiting Slave SYNC
    __delay_cycles(250);

    // SOFTWARE RESET - mode configuration
    UCB0CTL0 = 0;
    UCB0CTL1 = (0 + UCSWRST*1 );

    // clearing IFg /16.4.9/p447/SLAU144j
    // set by setting UCSWRST just before
    IFG2 &= ~(UCB0TXIFG | UCB0RXIFG);

    UCB0CTL0 |= ( UCMST | UCMODE_0 | UCSYNC );
    UCB0CTL0 &= ~( UCCKPH | UCCKPL | UCMSB | UC7BIT );
    UCB0CTL1 |= UCSSEL_2;

    UCB0BR0 = 0x0A;     // divide SMCLK by 10
    UCB0BR1 = 0x00;

    // SPI : Fonctions secondaires
    // MISO-1.6 MOSI-1.7 et CLK-1.5
    // Ref. SLAS735G p48,49
    P1SEL  |= ( SCK | DATA_OUT | DATA_IN);
    P1SEL2 |= ( SCK | DATA_OUT | DATA_IN);

    UCB0CTL1 &= ~UCSWRST;                                // activation USCI
    IE2 |= UCB0RXIE;
}


