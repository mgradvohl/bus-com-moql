/*
 * interpreter.c
 */

#include "interpreter.h"

/* ----------------------------------------------------------------------------
 * Fonction d'interpretation des commandes utilisateur
 * Entrees: -
 * Sorties:  -
 */

void interpreteur( void )
{
    if(strcmp((const char *)cmd, "h") == 0)          //----------------------------------- help
    {
        envoi_msg_UART("\r\nCommandes :");
        envoi_msg_UART("\r\n'ver' : version");
        envoi_msg_UART("\r\n'0' : LED off");
        envoi_msg_UART("\r\n'1' : LED on");
        envoi_msg_UART("\r\n'auto' : autonomous robot");
        envoi_msg_UART("\r\n'go' : robot go forward");
        envoi_msg_UART("\r\n'back' : robot go backward");
        envoi_msg_UART("\r\n'stop' : robot stop");
        envoi_msg_UART("\r\n'right' : robot turn right");
        envoi_msg_UART("\r\n'left' : robot turn left");
        envoi_msg_UART("\r\n'h' : help\r\n");
    }
    else if (strcmp((const char *)cmd, "0") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        Send_char_SPI(0x30); // Send '0' over SPI to Slave
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "1") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        Send_char_SPI(0x31); // Send '1' over SPI to Slave ASCII de 1
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "ver") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART(RELEASE);
        envoi_msg_UART("\r\n");
    }
    else if (strcmp((const char *)cmd, "go") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART("going forward");
        envoi_msg_UART("\r\n");
        go_forward();
    }
    else if (strcmp((const char *)cmd, "back") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART("going backward");
        envoi_msg_UART("\r\n");
        go_backward();
    }
    else if (strcmp((const char *)cmd, "stop") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART("stopping");
        envoi_msg_UART("\r\n");
        stop();
    }
    else if (strcmp((const char *)cmd, "right") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART("turning right");
        envoi_msg_UART("\r\n");
        right();
    }
    else if (strcmp((const char *)cmd, "left") == 0)
    {
        envoi_msg_UART("\r\n");
        envoi_msg_UART("turning left");
        envoi_msg_UART("\r\n");
        left();
    }
    else                          //---------------------------- default choice
    {
        envoi_msg_UART("\r\n ?");
        envoi_msg_UART((unsigned char *)cmd);
        envoi_msg_UART("->");
        Send_char_SPI((unsigned char *)cmd);
        envoi_msg_UART("\r\n");
        stop();

    }
    envoi_msg_UART(PROMPT);        //---------------------------- command prompt
}

/* ----------------------------------------------------------------------------
 * Fonction d'emission d'une chaine de caracteres
 * Entree : pointeur sur chaine de caracteres
 * Sorties:  -
 */
void envoi_msg_UART(const unsigned char *msg)
{
    unsigned int i = 0;
    for(i=0 ; msg[i] != 0x00 ; i++)
    {
        while(!(IFG2 & UCA0TXIFG));    //attente de fin du dernier envoi (UCA0TXIFG � 1 quand UCA0TXBUF vide)
        UCA0TXBUF=msg[i];
    }
}

/* ----------------------------------------------------------------------------
 * Fonction d'envoie d'un caract�re sur USCI en SPI 3 fils MASTER Mode
 * Entree : Caract�re � envoyer
 * Sorties: /
 */
void Send_char_SPI(unsigned char carac)
{
    while ((UCB0STAT & UCBUSY));   // attend que USCI_SPI soit dispo.
    while(!(IFG2 & UCB0TXIFG)); // p442
    UCB0TXBUF = carac;              // Put character in transmit buffer
    envoi_msg_UART((unsigned char *)cmd);   // slave echo
}
