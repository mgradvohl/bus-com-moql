/*
 * movements.h
 */

#ifndef MOVEMENTS_H_
#define MOVEMENTS_H_

#include <msp430.h>

/*
 * Fonction de movimentation du robot en direction vers l'avant
 */
void go_forward( void );

/*
 * Fonction de movimentation du robot en direction vers l'arriere
 */
void go_backward( void );

/*
 * Fonction d'arret du robot
 */
void stop ( void );

/*
 * Fonction de movimentation du robot en direction � droite
 */
void right( void );

/*
 * Fonction de movimentation du robot en direction � gauche
 */
void left ( void );

#endif /* MOVEMENTS_H_ */
