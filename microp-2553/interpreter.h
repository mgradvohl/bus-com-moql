/*
 * interpreter.h
 */

#ifndef INTERPRETER_H_
#define INTERPRETER_H_

#include <msp430.h>
#include <string.h>
#include "movements.h"

#define RELEASE "\r\t\tSPI-rIII162018"
#define PROMPT  "\r\nmaster>"
#define CMDLEN  10

unsigned char cmd[CMDLEN];      // tableau de caracteres lie a la commande user

/*
 * Fonction d'interpretation des messages re�us avec les interruptions
 * des communications UART et SPI du microprocessur MSP430G2553
 */
void interpreteur( void );

/*
 * Fonction d'envoi des messages par le protocole UART,
 * utilis� pour donner r�ponse aux messages re�us du module Bluetooth
 */
void envoi_msg_UART(unsigned char *msg);

/*
 * Fonction d'envoi des messages par le protocole UART,
 * utilis� pour donner r�ponse aux messages re�us du deuxieme microp
 */
void Send_char_SPI(unsigned char carac);

#endif /* INTERPRETER_H_ */
