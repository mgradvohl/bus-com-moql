/*
 * init.h
 */

#ifndef INIT_H_
#define INIT_H_

#include <msp430.h>

#define _CS         BIT4            // chip select for SPI Master->Slave ONLY on 4 wires Mode
#define SCK         BIT5            // Serial Clock
#define DATA_OUT    BIT6            // DATA out
#define DATA_IN     BIT7            // DATA in

#define LED_R       BIT0            // Red LED on Launchpad
#define LED_G       BIT6            // Green LED

/*
 * Fonction d'initialisation du microprocesseur et des broches principales
 * qui doivent �tre parametr� pour les fonctionnements desir�s
 */
void init_BOARD( void );

/*
 * Fonction d'initialisation des broches et parametres du
 * microprocesseur li�s au module de communication UART
 */
void init_UART( void );

/*
 * Fonction d'initialisation des broches et parametres du microprocesseur
 * li�s au module de communication USCI qui permet l'utilisation du SPI
 */
void init_USCI( void );

#endif /* INIT_H_ */
