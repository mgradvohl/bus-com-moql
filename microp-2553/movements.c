/*
 * Movements.c
 *
 */

#include "movements.h"


void go_forward()
{
    P2OUT  &= ~BIT1;
    P2OUT  |=  BIT5;
    TA1CCR1 = 100;               // determine le rapport cyclique du signal TA1.1 => moteur gauche
    TA1CCR2 = 100;               // determine le rapport cyclique du signal TA1.1 => moteur droit
    __delay_cycles(2000000);
    stop();
}

void go_backward()
{
    P2OUT  &= ~BIT5;
    P2OUT  |=  BIT1;
    TA1CCR1 = 100;               // determine le rapport cyclique du signal TA1.1 => moteur gauche
    TA1CCR2 = 100;               // determine le rapport cyclique du signal TA1.1 => moteur droit
    __delay_cycles(2000000);
    stop();
}

void stop()
{
    TA1CCR1 = 0;
    TA1CCR2 = 0;
}

void right()
{
    P2OUT&= ~BIT1;
    P2OUT&= ~BIT5;
    TA1CCR1 = 60;               // determine le rapport cyclique du signal TA1.1 => moteur gauche
    TA1CCR2 = 60;               // determine le rapport cyclique du signal TA1.1 => moteur droit
    __delay_cycles(600000);     // modifier selon vitesse robot fast
    stop();
}

void left()
{
    P2OUT|=BIT1;
    P2OUT|=BIT5;
    TA1CCR1 = 60;               // determine le rapport cyclique du signal TA1.1 => moteur gauche
    TA1CCR2 = 60;               // determine le rapport cyclique du signal TA1.1 => moteur droit
    __delay_cycles(600000);     // modifier selon vitesse robot fast
    stop();
}
